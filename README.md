# LibGDX Template project - 0110 Studios
## Steps to use template
1. Download project source
2. Open with android studio
4. From Android Studio
	1. Rename core package from 'com.studios0110.TemplateProject' to 'com.studios0110.YourApp'
		1. Right click page
		2. Refactor
		3. Rename
		4. Rename all once prompted
	2. Ctrl+Shift+R find .TemplateProject. and replace with .YourApp.
		1. Replace all occurences
	4. Go to android/AndroidManifest.xml 
		1. Line 15 should look like this: android:name="com.studios0110.YourApp.android.AndroidLauncher"
	3. Edit run configurations for new desktop launcher
		1. Run
		2. Edit configurations
		3. Find desktop launcher in .YourApp. package
		4. point working space to assets folder in android package