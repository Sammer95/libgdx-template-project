package com.studios0110.TemplateProject.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.studios0110.TemplateProject.Splash.Splash;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Splash.screenW;
		config.height = Splash.screenH;
		new LwjglApplication(new com.studios0110.TemplateProject.Splash.Starter(), config);
	}
}
