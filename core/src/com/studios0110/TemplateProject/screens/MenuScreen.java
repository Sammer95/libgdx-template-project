package com.studios0110.TemplateProject.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.studios0110.TemplateProject.Splash.Splash;

/*
Sam Merante: This Starter Screen is a menu to get to other screens.
 */
public class MenuScreen implements NewScreenInterface {



    private BitmapFont font;
    private float showFPSDelay,FPS;
    private float width = Splash.screenW, height = Splash.screenH;
    boolean showDebug;
   // Button start;
    private Texture menuScreen;
    public void show() {

        System.out.println("Construct Menu Screen");
        Splash.camera.zoom=1;
        Splash.camera.update();
        font = Splash.manager.get("Fonts/Font.fnt");
        showDebug = true;
        FPS = 60;
      /* menuScreen = Splash.manager.get("ui/menuScreen.png");
        Gdx.input.setInputProcessor(mp); // set the default input processor to the multiplex processor from NewScreenInterface
        Gdx.input.setCatchBackKey(true);
		 start = new Button("playButton",new Vector2((width/2)-256,(height/2)-64-200));
		 mp.addProcessor(new GestureDetector(start));*/


    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(Splash.camera.combined);
        shapes.setProjectionMatrix(Splash.camera.combined);
        shapes.setAutoShapeType(true);

        showFPS(delta);
    }
    private void showFPS(float delta)
    {
            showDebug = (Gdx.input.isKeyJustPressed(Input.Keys.F1) ? !showDebug: showDebug);
            if(showDebug) {
                showFPSDelay += 1 * Gdx.graphics.getDeltaTime();
                if (showFPSDelay >= 0.25) {
                    showFPSDelay = 0;
                    FPS = (int) Math.ceil(1 / delta);
                }
                    batch.begin();
                    font.draw(batch, "Menu - FPS " + (int) FPS, width/2, height-20);
                    batch.end();
            }

    }

    @Override
    public void resize(int width, int height) {}
    @Override
    public void pause() {}
    @Override
    public void resume() {}
    @Override
    public void hide() {}
    @Override
    public void dispose() {}

}
